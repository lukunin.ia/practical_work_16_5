#include <iostream>
#include <time.h>

using namespace std;

const int N = 5; //  ������ �������

int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	cout << "Current day:" << buf.tm_mday << '\n' << endl; // ����� �������� ����� ������

	int str = buf.tm_mday % N; // ����� ������ ������ ������� ������� �������� ����� ��������� �� N
	//cout << str << '\n' << endl; // ������� ����� ������ ��� ��������

	int array[N][N];
	int sum = 0;
	for (int i = 0; i < N; i++) { // ����� ������
		for (int j = 0; j < N; j++) { // ����� �������
			array[i][j] = i + j;
			cout.width(2);
			cout << array[i][j]; // ������� ������ � �������

			if (i == str) // ���������� ������ ������
			{
				sum += array[i][j]; // ����� ��������� � ������ �������
			}
		}
		cout << endl;
	}
	cout << '\n' << sum;
	return 0;
}
